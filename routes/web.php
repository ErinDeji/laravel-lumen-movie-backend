<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// users routes

$router->get('/users', 'UserController@index');
$router->post('/users', 'UserController@store');
$router->get('/users/{user}', 'UserController@show');
$router->put('/users/{user}', 'UserController@update');
$router->patch('/users/{user}', 'UserController@update');
$router->delete('/users/{user}', 'UserController@destroy');


// actors routes

$router->get('/actors', 'ActorController@index');
$router->post('/actors', 'ActorController@store');
$router->get('/actors/{actor}', 'ActorController@show');
$router->put('/actors/{actor}', 'ActorController@update');
$router->patch('/actors/{actor}', 'ActorController@update');
$router->delete('/actors/{actor}', 'ActorController@destroy');


// categories routes

$router->get('/categories', 'CategoryController@index');
$router->post('/categories', 'CategoryController@store');
$router->get('/categories/{category}', 'CategoryController@show');
$router->put('/categories/{category}', 'CategoryController@update');
$router->patch('/categories/{category}', 'CategoryController@update');
$router->delete('/categories/{category}', 'CategoryController@destroy');


// comments routes

$router->get('/comments', 'CommentController@index');
$router->post('/comments', 'CommentController@store');
$router->get('/comments/{comment}', 'CommentController@show');
$router->put('/comments/{comment}', 'CommentController@update');
$router->patch('/comments/{comment}', 'CommentController@update');
$router->delete('/comments/{comment}', 'CommentController@destroy');


// movies routes

$router->get('/movies', 'MovieController@index');
$router->post('/movies', 'MovieController@store');
$router->get('/movies/{movie}', 'MovieController@show');
$router->put('/movies/{movie}', 'MovieController@update');
$router->patch('/movies/{movie}', 'MovieController@update');
$router->delete('/movies/{movie}', 'MovieController@destroy');


// ratings routes

$router->get('/ratings', 'RatingController@index');
$router->post('/ratings', 'RatingController@store');
$router->get('/ratings/{rating}', 'RatingController@show');
$router->put('/ratings/{rating}', 'RatingController@update');
$router->patch('/ratings/{rating}', 'RatingController@update');
$router->delete('/ratings/{rating}', 'RatingController@destroy');