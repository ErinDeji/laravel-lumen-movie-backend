<?php

namespace App\Http\Controllers;

use App\Category;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Categorys
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return $this->successResponse($categories);
    }

    /**
     * Create one new Category
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'movie_id' => 'required|max:10',
            'actor_id' => 'required|max:10',
            'comments' => 'required|max:255',
        ];

        $this->validate($request, $rules);

        $category = Category::create($request->all());

        return $this->successResponse($category, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Category
     *@return Illuminate\Http\Response
     */
    public function show($category)
    {
        $category = Category::findOrFail($category);
        return $this->successResponse($category);
    }

    /**
     * Update an existing Category
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        $rules = [
            'movie_id' => 'max:10',
            'actor_id' => 'max:10',
            'comments' => 'max:255',
        ];

        $this->validate($request, $rules);
        $category = Category::findOrFail($category);
        $category->fill($request->all());
        if ($category->isClean()) {
            return $this->errorResponse(
                'At least one value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $category->save();
        return $this->successResponse($category);
    }

    /**
     *  Delete an existing Category with id
     *@return Illuminate\Http\Response
     */
    public function destroy($category)
    {
        $category = Category::findOrFail($category);

        $category->delete();
        return $this->successResponse('Deleted Successfully');
    }
}