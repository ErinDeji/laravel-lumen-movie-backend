<?php

namespace App\Http\Controllers;

use App\Rating;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class RatingController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Ratings
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $ratings = Rating::all();
        return $this->successResponse($ratings);
    }

    /**
     * Create one new Rating
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required|max:10',
            'ratings' => 'required|max:255',
        ];

        $this->validate($request, $rules);

        $rating = Rating::create($request->all());

        return $this->successResponse($rating, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Rating
     *@return Illuminate\Http\Response
     */
    public function show($rating)
    {
        $rating = Rating::findOrFail($rating);
        return $this->successResponse($rating);
    }

    /**
     * Update an existing Rating
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $rating)
    {
        $rules = [
            'user_id' => 'max:10',
            'ratings' => 'max:255',
        ];

        $this->validate($request, $rules);
        $rating = Rating::findOrFail($rating);
        $rating->fill($request->all());
        if ($rating->isClean()) {
            return $this->errorResponse(
                'At least one value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $rating->save();
        return $this->successResponse($rating);
    }

    /**
     *  Delete an existing Rating with id
     *@return Illuminate\Http\Response
     */
    public function destroy($rating)
    {
        $rating = Rating::findOrFail($rating);

        $rating->delete();
        return $this->successResponse('Deleted Successfully');
    }
}