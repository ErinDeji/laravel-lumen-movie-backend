<?php

namespace App\Http\Controllers;

use App\Actor;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ActorController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Authors
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $actors = Actor::all();
        return $this->successResponse($actors);
    }

    /**
     * Create one new Actor
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'movie_id' => 'required|max:10',
            'actor_name' => 'required|max:255',
        ];

        $this->validate($request, $rules);

        $actor = Actor::create($request->all());

        return $this->successResponse($actor, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Actor
     *@return Illuminate\Http\Response
     */
    public function show($actor)
    {
        $actor = Actor::findOrFail($actor);
        return $this->successResponse($actor);
    }

    /**
     * Update an existing Actor
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $actor)
    {
        $rules = [
            'movie_id' => 'max:10',
            'actor_name' => 'max:255',
        ];

        $this->validate($request, $rules);
        $actor = Actor::findOrFail($actor);
        $actor->fill($request->all());
        if ($actor->isClean()) {
            return $this->errorResponse(
                'At least one value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $actor->save();
        return $this->successResponse($actor);
    }

    /**
     *  Delete an existing Actor with id
     *@return Illuminate\Http\Response
     */
    public function destroy($actor)
    {
        $actor = Actor::findOrFail($actor);

        $actor->delete();
        return $this->successResponse('Deleted Successfully');
    }
}