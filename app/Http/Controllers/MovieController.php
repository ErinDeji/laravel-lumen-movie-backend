<?php

namespace App\Http\Controllers;

use App\Movie;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Movies
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return $this->successResponse($movies);
    }

    /**
     * Create one new Movie
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'rating_id' => 'required|max:15'
        ];

        $this->validate($request, $rules);

        $movie = Movie::create($request->all());

        return $this->successResponse($movie, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Movie
     *@return Illuminate\Http\Response
     */
    public function show($movie)
    {
        $movie = Movie::findOrFail($movie);
        return $this->successResponse($movie);
    }

    /**
     * Update an existing Movie
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $movie)
    {
        $rules = [
            'rating_id' => 'max:15'
        ];

        $this->validate($request, $rules);
        $movie = Movie::findOrFail($movie);
        $movie->fill($request->all());
        if ($movie->isClean()) {
            return $this->errorResponse(
                'At least one value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $movie->save();
        return $this->successResponse($movie);
    }

    /**
     *  Delete an existing Movie with id
     *@return Illuminate\Http\Response
     */
    public function destroy($movie)
    {
        $movie = Movie::findOrFail($movie);

        $movie->delete();
        return $this->successResponse('Deleted Successfully');
    }
}