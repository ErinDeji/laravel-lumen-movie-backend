<?php

namespace App\Http\Controllers;

use App\Comment;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Comments
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();
        return $this->successResponse($comments);
    }

    /**
     * Create one new Comment
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required|max:10',
            'comments' => 'required|max:255',
        ];

        $this->validate($request, $rules);

        $comment = Comment::create($request->all());

        return $this->successResponse($comment, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Comment
     *@return Illuminate\Http\Response
     */
    public function show($comment)
    {
        $comment = Comment::findOrFail($comment);
        return $this->successResponse($comment);
    }

    /**
     * Update an existing Comment
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $comment)
    {
        $rules = [
            'user_id' => 'max:10',
            'comment' => 'max:255'
        ];

        $this->validate($request, $rules);
        $comment = Comment::findOrFail($comment);
        $comment->fill($request->all());
        if ($comment->isClean()) {
            return $this->errorResponse(
                'At least one value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $comment->save();
        return $this->successResponse($comment);
    }

    /**
     *  Delete an existing Comment with id
     *@return Illuminate\Http\Response
     */
    public function destroy($comment)
    {
        $comment = Comment::findOrFail($comment);

        $comment->delete();
        return $this->successResponse('Deleted Successfully');
    }
}